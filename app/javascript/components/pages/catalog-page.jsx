import React, {Fragment, PureComponent} from 'react';
import Header from "../items/header";
import Footer from "../items/footer";
import MainContent from "../items/main-content";
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {Operations} from "../../reducers/catalog/catalog";
import Catalog from "../items/catalog";
import {Urls} from "../../utils";
import AnimationBase from "../items/animation-base";

class CatalogPage extends PureComponent {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const {getGoodsCatalog} = this.props;
        getGoodsCatalog();
    }

    render() {
        const {goodsCatalog} = this.props;

        if (!goodsCatalog) {
            return (
                <Fragment>
                    <Header/>
                    <MainContent>
                        <div className="container grid-container">
                            <h1 className="h1-title">
                                <AnimationBase>Каталог</AnimationBase>
                            </h1>
                        </div>
                    </MainContent>
                    <Footer/>
                </Fragment>
            );
        }

        const streetGoods = goodsCatalog.filter((goodsItem) => goodsItem.group === 0);
        const otherGoods = goodsCatalog.filter((goodsItem) => goodsItem.group === 1);

        return (
            <Fragment>
                <Header/>
                <MainContent>
                    <div className="container grid-container">
                        <h1 className="h1-title">
                            <AnimationBase>Каталог</AnimationBase>
                        </h1>
                        {streetGoods.length > 0 && <Catalog data={streetGoods} catalogCategory="Серии уличной мебели"/>}
                        {otherGoods.length > 0 && <Catalog data={otherGoods} catalogCategory="Другая продукция"/>}
                    </div>
                </MainContent>
                <Footer/>
            </Fragment>
        );
    }
};

const mapStateToProps = (state) => {
    return {
        goodsCatalog: state.catalog.goodsCatalog
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getGoodsCatalog: bindActionCreators(Operations.getGoodsCatalog, dispatch),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CatalogPage);
