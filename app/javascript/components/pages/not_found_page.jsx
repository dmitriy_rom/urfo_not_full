import React, {Fragment} from 'react';
import Header from "../items/header";
import MainContent from "../items/main-content";

const NotFoundPage = () => {
    return (
        <div className="green-theme fix-min-height">
            <Header/>
            <MainContent>
                <div className="container grid-container">
                    <div className="title-sup">Ошибка 404</div>
                    <h1 className="h1-title">Страница не найдена</h1>
                </div>
            </MainContent>
        </div>
    );
};

export default NotFoundPage;
