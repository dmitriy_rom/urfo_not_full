import React, {Fragment, PureComponent} from 'react';
import Header from "../items/header";
import MainSlider from "../items/main-slider";
import Footer from "../items/footer";
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {Operations} from "../../reducers/series/series";
import {Operations as InstaOperations} from "../../reducers/instagram/instagram";
import MainContent from "../items/main-content";
import Catalog from "../items/catalog";
import {Urls} from "../../utils";
import {Link} from 'react-router-dom'
import MainSliderMobile from "../items/main-slider-mobile";
import CatalogSliderMobile from "../items/catalog-slider-mobile";
import AnimationBase from "../items/animation-base";

class HomePage extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            currentWidthWindow: window.innerWidth
        };

        this.resizeWindowHandler = this.resizeWindowHandler.bind(this);
    }

    resizeWindowHandler() {
        this.setState({
            currentWidthWindow: window.innerWidth
        });
    }

    componentDidMount() {
        const {getSmallHomeSeries, getBigHomeSeries, getInstagramMedia} = this.props;
        getSmallHomeSeries();
        getBigHomeSeries();
        getInstagramMedia();

        window.addEventListener('resize', this.resizeWindowHandler);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.resizeWindowHandler);
    }

    render() {
        const {smallHomeSeries, bigHomeSeries, instagramMedia, contacts} = this.props;
        const {currentWidthWindow} = this.state;

        const BREAK_POINT_FOR_SLIDER_MIBILE = 1100;
        const BREAK_POINT_FOR_MARGIN_BOTTOM = 900;
        const BREAK_POINT_FOR_CARDS_SLIDER_MOBILE = 600;

        const marginBottomSection = currentWidthWindow > BREAK_POINT_FOR_MARGIN_BOTTOM ? '12.4rem' : '9.6rem';

        return (
            <Fragment>
                <Header/>
                <MainContent>
                    <div className="container grid-container">
                        <h1 className="visually-hidden">Официальный сайт urfo</h1>
                        <AnimationBase classDiv="home-slogan">
                            Мебель для города нового поколения
                        </AnimationBase>
                    </div>
                    {currentWidthWindow >= BREAK_POINT_FOR_SLIDER_MIBILE && <div className="grid-main-slider-container" style={{marginBottom: `24.8rem`}}>
                        <MainSlider/>
                    </div>}
                    {currentWidthWindow < BREAK_POINT_FOR_SLIDER_MIBILE && <MainSliderMobile/>}
                    <div className="container grid-container" style={{marginBottom: marginBottomSection}}>
                        <AnimationBase classDiv="promo-text-center">
                            Придумываем и производим городскую мебель: помогаем общественным пространствам быть в центре внимания
                        </AnimationBase>
                    </div>
                    <div className="container grid-container cards-mobile-slider" style={{marginBottom: marginBottomSection}}>
                        {currentWidthWindow >= BREAK_POINT_FOR_CARDS_SLIDER_MOBILE && <Catalog data={smallHomeSeries}/>}
                        {currentWidthWindow < BREAK_POINT_FOR_CARDS_SLIDER_MOBILE && <CatalogSliderMobile slides={smallHomeSeries}/>}
                    </div>
                    <div className="container grid-container">
                        <Catalog data={bigHomeSeries} vertical={true}/>
                        <AnimationBase classDiv="promo-text-center">
                            Вы можете посмотреть все серии мебели в нашем <Link to={`/${Urls.CATALOG}`} className="accent-in-text">каталоге</Link>
                        </AnimationBase>
                        <AnimationBase classDiv="social-photo">
                            <div className="social-photo__title">
                                Следите за нами <a className="link-in-text" href={contacts.instaLink} target="_blank">Instagram</a> и&nbsp;
                                <a className="link-in-text" href={contacts.facebookLink} target="_blank">facebook</a>
                            </div>
                            <div className="social-photo__list">
                                {instagramMedia.map((image, index) => {
                                    return <div
                                        key={`instaMedia-${index}`}
                                        className="social-photo__item"
                                    >
                                        <a href={image.postUrl} target="_blank">
                                            <img src={image.imageUrl} className="social-photo__image"/>
                                        </a>
                                    </div>;
                                })}
                            </div>
                        </AnimationBase>
                    </div>
                </MainContent>
                <Footer/>
            </Fragment>
        );
    }
};

const mapStateToProps = (state) => {
    return {
        smallHomeSeries: state.series.smallHomeSeries,
        bigHomeSeries: state.series.bigHomeSeries,
        instagramMedia: state.instagram.instagramMedia,
        contacts: state.general.contacts
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getSmallHomeSeries: bindActionCreators(Operations.getSmallHomeSeries, dispatch),
        getBigHomeSeries: bindActionCreators(Operations.getBigHomeSeries, dispatch),
        getInstagramMedia: bindActionCreators(InstaOperations.getInstagramMedia, dispatch)
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
