import React, {Fragment, PureComponent} from 'react';
import Header from "../items/header";
import Footer from "../items/footer";
import MainContent from "../items/main-content";
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {Operations} from "../../reducers/projects/projects";
import ProjectsList from "../items/projects_list";
import {Urls} from "../../utils";
import AnimationBase from "../items/animation-base";

class ProjectsPage extends PureComponent {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const {getProjects} = this.props;
        getProjects();
    }

    render() {
        const {projects} = this.props;

        return(
            <Fragment>
                <Header/>
                <MainContent>
                    <div className="container grid-container">
                        <h1 className="h1-title">
                            <AnimationBase>Кейсы</AnimationBase>
                        </h1>
                        <ProjectsList projects={projects}/>
                    </div>
                </MainContent>
                <Footer/>
            </Fragment>
        );
    }
};

const mapStateToProps = (state) => {
    return {
        projects: state.projects.projects
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getProjects: bindActionCreators(Operations.getProjects, dispatch),
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ProjectsPage);
