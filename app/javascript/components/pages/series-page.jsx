import React, {Fragment, PureComponent} from 'react';
import Header from "../items/header";
import MainContent from "../items/main-content";
import Footer from "../items/footer";
import CardInfo from "../items/card-info";
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {Link} from 'react-router-dom';

import ProjectsList from "../items/projects_list";
import NextCard from "../items/next-card";
import {Operations as OperationsSeries, ActionCreator} from "../../reducers/series/series";
import {Operations as OperationsProjects} from "../../reducers/projects/projects";
import {Urls} from "../../utils";
import {Helmet} from "react-helmet"
import AnimationBase from "../items/animation-base";

class SeriesPage extends PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            currentWidthWindow: window.innerWidth
        };
        this.resizeWindowHandler = this.resizeWindowHandler.bind(this);
    }

    resizeWindowHandler() {
        this.setState({
            currentWidthWindow: window.innerWidth
        });
    }

    componentDidMount() {
        const {getSeriesPageInfo, getSeriesProject, slug} = this.props;
        getSeriesPageInfo(slug);
        getSeriesProject(slug);
        window.addEventListener('resize', this.resizeWindowHandler);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.resizeWindowHandler);
        const {clearSeriesPageInfo} = this.props;
        clearSeriesPageInfo();
    }

    componentWillReceiveProps(nextProps) {
        const {slug} = this.props;
        if (nextProps.slug !== slug) {
            const {getSeriesPageInfo, getSeriesProject} = this.props;
            getSeriesPageInfo(nextProps.slug);
            getSeriesProject(nextProps.slug);
        }
    }

    render() {
        const {seriesPageInfo, seriesProject} = this.props;
        const {currentWidthWindow} = this.state;

        if (!seriesPageInfo) {
            return (
                <Fragment>
                    <Header isActive={true}/>
                    <Footer/>
                </Fragment>
            );
        }

        const {title, description, materials, vertical, imageUrl, goods, next_series, seo} = seriesPageInfo;
        const verticalClass = vertical ? ` card-series--vertical` : ``;
        const twoColumnClass = ((goods.length > 3) || currentWidthWindow < 900)? ` two-column` : ``;

        return (
            <Fragment>
                <Header isActive={true}/>
                <MainContent>
                    <Helmet>
                        <meta charSet="utf-8" />
                        <title>{seo.title}</title>
                        <meta name="description" content={seo.description} />
                        <meta name="keywords" content={seo.keywords} />
                    </Helmet>
                    {currentWidthWindow < 900 && <div className="container grid-container">
                        <div className="card-series__title">{title}</div>
                    </div>}
                    <div className={`card-series container grid-container${verticalClass}${twoColumnClass}`} style={{marginBottom: `12.4rem`}}>
                        <AnimationBase classDiv="card-series__img-box">
                            <img src={imageUrl} className="card-series__img"/>
                        </AnimationBase>
                        <AnimationBase classDiv="card-series__content">
                            <CardInfo
                                title={title}
                                description={description}
                                materials={materials}
                            />
                        </AnimationBase>
                        <AnimationBase classDiv={`card-series__goods goods-series${twoColumnClass}`}>
                            {goods.map((good, index) => {
                                return <Link
                                    key={`good-${index}`}
                                    to={`/${Urls.GOODS}/${good.slug}`}
                                    className="goods-series__item"
                                >
                                    <div className="goods-series__title">{good.title}</div>
                                    <img className="goods-series__img" src={good.imageUrl}/>
                                </Link>;
                            })}
                        </AnimationBase>
                    </div>

                    {seriesProject.length > 0 && <div className="container grid-container">
                        <ProjectsList projects={seriesProject}/>
                    </div>}

                </MainContent>
                <NextCard data={next_series}/>
                <Footer/>
            </Fragment>
        );
    }
};

const mapStateToProps = (state) => ({
    seriesPageInfo: state.series.seriesPageInfo,
    seriesProject: state.projects.seriesProject
});

const mapDispatchToProps = (dispatch) => ({
    getSeriesPageInfo: bindActionCreators(OperationsSeries.getSeriesPageInfo, dispatch),
    clearSeriesPageInfo: () => dispatch(ActionCreator.clearSeriesPageInfo()),
    getSeriesProject: bindActionCreators(OperationsProjects.getSeriesProject, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(SeriesPage);
