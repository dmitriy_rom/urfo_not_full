import React, {Fragment, Component} from 'react';
import Header from "../items/header";
import MainContent from "../items/main-content";
import Footer from "../items/footer";
import CardInfo from "../items/card-info";
import Tooltipe from "../items/tooltipe";
import FormGoodsCard from "../items/form-goods-card";
import {Link} from 'react-router-dom';

import {connect} from 'react-redux';
import {ActionCreator, Operations} from "../../reducers/goods/goods";
import {ActionCreator as feedbackActionCreator} from "../../reducers/feedback/feedback";
import {bindActionCreators} from "redux";
import {Urls} from "../../utils";
import {Helmet} from "react-helmet"
import AnimationRight from "../items/animation-right";

class GoodsPage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isShowTooltipe: false,
            currentMainImgShow: ``
        };

        this.closeTooltipe = this.closeTooltipe.bind(this);
        this.openTooltipe = this.openTooltipe.bind(this);
        this.setCurrentMainImg = this.setCurrentMainImg.bind(this);
    }

    closeTooltipe() {
        this.setState({
            isShowTooltipe: false
        });
    }

    openTooltipe() {
        this.setState({
            isShowTooltipe: true
        });
    }

    componentDidMount() {
        const {slug} = this.props;
        const {getGoodsPageInfo} = this.props;

        getGoodsPageInfo(slug);
    }

    componentWillUnmount() {
        const {clearGoodsPageInfo} = this.props;
        clearGoodsPageInfo();
    }

    setCurrentMainImg(color) {
        this.setState({
            currentMainImgShow: color.imageUrl
        });
    }

    render() {
        const {goodsPageInfo, openFeedbackPopup} = this.props;

        if (!goodsPageInfo) {
            return (
                <Fragment>
                    <Header isActive={true}/>
                    <MainContent>

                    </MainContent>
                    <Footer/>
                </Fragment>
            );
        }

        const {isShowTooltipe, currentMainImgShow} = this.state;

        const {title, materials, dimensions, seriesSlug, seo} = goodsPageInfo;

        const urlGoBack = seriesSlug !== null ? `${Urls.SERIES}/${seriesSlug}` : Urls.CATALOG;

        return (
            <Fragment>
                <Header isActive={true}/>
                <MainContent>
                    <Helmet>
                        <meta charSet="utf-8" />
                        <title>{seo.title}</title>
                        <meta name="description" content={seo.description} />
                        <meta name="keywords" content={seo.keywords} />
                    </Helmet>
                    <AnimationRight classDiv="card-goods container grid-container">
                        <div className="card-goods__img-box">
                            <img src={currentMainImgShow} className="card-goods__img"/>
                            <Link to={`/${urlGoBack}`}  className="card-goods__back">
                                <svg width="19" height="32" viewBox="0 0 19 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M18.2008 0.799217L2.70078 12.5992C0.500781 14.2992 0.500781 17.6992 2.70078 19.3992L18.2008 31.1992" stroke="currentColor" strokeWidth="2" strokeMiterlimit="10" strokeLinejoin="round"/>
                                </svg>
                                <div>Назад</div>
                            </Link>
                        </div>
                        <div className="card-goods__content">
                            <CardInfo title={title} materials={materials} dimensions={dimensions}/>
                            <Tooltipe
                                isShow={isShowTooltipe}
                                closeTooltipe={this.closeTooltipe}
                            >
                                Мы можем покрасить нашу мебель в любой необходимый Вам цвет, <a href="#" className="link" onClick={openFeedbackPopup}>свяжитесь с нами</a> и мы всё обсудим
                            </Tooltipe>
                            <FormGoodsCard
                                isShowTooltipe={isShowTooltipe}
                                openTooltipe={this.openTooltipe}
                                goodsPageInfo={goodsPageInfo}
                                setCurrentMainImg={this.setCurrentMainImg}
                            />
                        </div>
                    </AnimationRight>
                </MainContent>
                <Footer/>
            </Fragment>
        );
    }
};

const mapStateToProps = (state) => ({
    goodsPageInfo: state.goods.goodsPageInfo
});

const mapDispatchToProps = (dispatch) => ({
    getGoodsPageInfo: bindActionCreators(Operations.getGoodsPageInfo, dispatch),
    clearGoodsPageInfo: () => dispatch(ActionCreator.clearGoodsPageInfo()),
    openFeedbackPopup: () => dispatch(feedbackActionCreator.openFeedbackPopup())
});

export default connect(mapStateToProps, mapDispatchToProps)(GoodsPage);
