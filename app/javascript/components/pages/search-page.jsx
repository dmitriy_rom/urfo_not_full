import React, {PureComponent} from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';
import AnimationBase from "../items/animation-base";

class SearchPage extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            catalogItems: [],
            textInput: '',
            isResultsShow: false
        };

        this.textInput = React.createRef();
        this.onChangeHandler = this.onChangeHandler.bind(this);
    }

    componentDidMount() {
        axios.get(`/api/get_catalog_items`)
            .then(({data}) => this.setState({catalogItems: data}))
            .catch((err) => console.log(err.message));
        this.textInput.current.focus();
    }

    onChangeHandler(evt) {
        const {isResultsShow} = this.state;

        if (!isResultsShow) {
            this.setState({
                isResultsShow: true
            });
        }

        this.setState({
            textInput: evt.target.value
        });
    }

    render() {
        const {catalogItems, textInput, isResultsShow} = this.state;

        const filterCatalogItemsFn = (item) => {
            return item.title.toLowerCase().includes(textInput.toLowerCase());
        };

        const accentWordTitleFn = (string) => {
            const positionSubstr = string.toLowerCase().indexOf(textInput.toLowerCase());

            if (positionSubstr === -1) {
                return string;
            }

            return {__html: `${string.slice(0, positionSubstr)}<span style="color: #FDB913;">${string.slice(positionSubstr, positionSubstr + textInput.length)}</span>${string.slice(positionSubstr + textInput.length)}`};
        };

        return (
            <div className={`search-page${isResultsShow ? `` : ` bg-emerald`}`}>
                <div className="search-page__header container grid-container">
                    <Link to={`/`} className="search-page__close">
                        <svg width="40" height="32" viewBox="0 0 40 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1 30.45L16.6202 19.0187C18.8372 17.3719 18.8372 14.0781 16.6202 12.4312L1 1" stroke="currentColor" strokeWidth="2" strokeMiterlimit="10" strokeLinejoin="round"/>
                            <path d="M38.79 1L23.069 12.4312C20.852 14.0781 20.852 17.3719 23.069 19.0187L38.6892 30.45" stroke="currentColor" strokeWidth="2" strokeMiterlimit="10" strokeLinejoin="round"/>
                        </svg>
                    </Link>
                    <input
                        type="text"
                        className="search-page__input"
                        value={textInput}
                        placeholder="Поиск"
                        onChange={this.onChangeHandler}
                        ref={this.textInput}
                    />
                </div>
                {isResultsShow && <div className="container grid-container">
                    {textInput.length > 0 && catalogItems.filter(filterCatalogItemsFn).length > 0 && <div className="search-page__title">Каталог</div>}

                    <div className="search-page__catalog">
                        {textInput.length > 0 && catalogItems.filter(filterCatalogItemsFn).map((good, index) => {
                            return <AnimationBase
                                key={`good-${index}`}
                                classDiv="catalog__item"
                            >

                                <Link to={`/${good.model}/${good.slug}`} className="catalog__img">
                                    <img src={good.imageUrl} alt={good.title}/>
                                </Link>
                                <Link to={`/${good.model}/${good.slug}`} className="catalog__title">
                                    <div dangerouslySetInnerHTML={accentWordTitleFn(good.title)} />
                                    {/*{accentWordTitleFn(good.title)}*/}
                                </Link>
                                <div className="catalog__desc">{good.shortDescription}</div>

                            </AnimationBase>;
                        })}
                        {catalogItems.filter(filterCatalogItemsFn).length === 0 && <div>Поиск не дал результата.</div>}
                        {textInput.length === 0 && <div>Начните вводить название товара</div>}
                    </div>
                </div>}
            </div>
        );
    }
};

export default SearchPage;
