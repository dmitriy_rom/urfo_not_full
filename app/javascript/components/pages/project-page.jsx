import React, {Fragment, PureComponent} from 'react';
import Header from "../items/header";
import Footer from "../items/footer";
import MainContent from "../items/main-content";
import VitrageContent from "../items/vitrage-content";
import Catalog from "../items/catalog";
import axios from "axios/index";
import {Helmet} from "react-helmet"
import NextCard from "../items/next-card";

class ProjectPage extends PureComponent {
    constructor(props) {
        super(props);
        this.getRelatedSeries = this.getRelatedSeries.bind(this);
        this.getSeo = this.getSeo.bind(this);
        this.getNextProject = this.getNextProject.bind(this);
        this.state = {relatedSeries: [], seo: {}, nextProject: {}};
    }

    componentDidMount() {
        this.getSeo();
    }

    getRelatedSeries() {
        const {slug} = this.props;

        axios.get(`/api/get_project_series/${slug}`)
            .then((res) => {
            this.setState({relatedSeries: res.data});
        });
    }

    getNextProject() {
        const {slug} = this.props;

        axios.get(`/api/get_next_project/${slug}`)
            .then((res) => {
                this.setState({nextProject: res.data});
            });
    }

    getSeo() {
        const {slug} = this.props;

        axios.get(`/api/get_project_seo/${slug}`)
            .then((res) => {
                this.setState({seo: res.data});
            });
    }

    render() {
        const {slug} = this.props;

        return (
            <Fragment>
                <Header/>
                <MainContent>
                    <Helmet>
                        <meta charSet="utf-8" />
                        <title>{this.state.seo.title}</title>
                        <meta name="description" content={this.state.seo.description} />
                        <meta name="keywords" content={this.state.seo.keywords} />
                    </Helmet>
                    <VitrageContent url={`/api/get_project_info/${slug}`} getRelatedSeries={this.getRelatedSeries} getNextProject={this.getNextProject}/>
                    <div className="container grid-container">
                        {this.state.relatedSeries.length > 0 && <Catalog data={this.state.relatedSeries} catalogCategory="Посмотрите эту мебель в каталоге"/>}
                    </div>
                </MainContent>
                {Object.keys(this.state.nextProject).length > 0 && <NextCard data={this.state.nextProject}/>}
                <Footer/>
            </Fragment>
        );
    }
};

export default ProjectPage;
