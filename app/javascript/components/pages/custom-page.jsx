import React, {Fragment, PureComponent} from 'react';
import Header from "../items/header";
import Footer from "../items/footer";
import MainContent from "../items/main-content";
import VitrageContent from "../items/vitrage-content";
import axios from "axios/index";
import {Helmet} from "react-helmet"

class CustomPage extends PureComponent {
    constructor(props) {
        super(props);
        this.getSeo = this.getSeo.bind(this);
        this.state = {seo: {}};
    }

    componentDidMount() {
        this.getSeo();
    }

    getSeo() {
        const {slug} = this.props;
        axios.get(`/api/get_page_seo/${slug}`)
            .then((res) => {
                this.setState({seo: res.data});
            })
            .catch((error) => {
                console.log("error", error);
            });
    }

    render() {
        const {slug} = this.props;

        return (
            <Fragment>
                <Header/>
                <MainContent>
                    <Helmet>
                        <meta charSet="utf-8" />
                        <title>{this.state.seo.title}</title>
                        <meta name="description" content={this.state.seo.description} />
                        <meta name="keywords" content={this.state.seo.keywords} />
                    </Helmet>
                    <VitrageContent url={`/api/get_page_info/${slug}`} getRelatedSeries={() => {}} getNextProject={() => {}}/>
                </MainContent>
                <Footer/>
            </Fragment>
        );
    }
};

export default CustomPage;