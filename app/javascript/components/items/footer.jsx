import React from 'react';
import Menu from "./menu";
import Socials from "./socials";
import FooterContacts from "./footer-contacts";

const Footer = () => {
    return(
        <footer className="footer">
            <div className="container">
                <div className="footer__top grid-container">
                    <div className="footer__socials socials">
                        <Socials />
                    </div>
                    <nav className="footer__nav nav">
                        <Menu/>
                    </nav>
                    <FooterContacts />
                </div>
                <div className="grid-container">
                    <div className="footer__bottom">
                        <div className="footer__text">
                            © URFO, 2020. Все права защищены
                        </div>
                        <div className="footer__text right">
                            <span>Сделано в&nbsp;</span>
                            <a href="http://proektmarketing.ru/" target="_blank" className="footer__proektmarketing">
                                proektmarketing
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    )
};

export default Footer;
