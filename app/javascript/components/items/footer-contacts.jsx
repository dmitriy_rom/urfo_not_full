import React from 'react';
import {connect} from 'react-redux'

const FooterContacts = ({contacts}) => {
    return <div className="footer__contacts">
        <div className="footer__text">
            Контактный телефон
        </div>
        <a href={`tel:${contacts.phone}`} className="footer__tel">{contacts.phone}</a>
    </div>;
};

const mapStateToProps = (state) => {
    return {
        contacts: state.general.contacts
    }
};

export default connect(mapStateToProps)(FooterContacts);
