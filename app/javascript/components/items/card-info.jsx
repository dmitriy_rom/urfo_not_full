import React from 'react';

const CardInfo = (props) => {
    const {title, description = false, materials, dimensions = false} = props;

    return (
        <div className="card-info">
            <div className="card-info__title">{title}</div>
            {description && <div className="card-info__desc">{description}</div>}
            {dimensions && <div className="card-info__desc">Ширина x высота x глубина:</div>}
            {dimensions && <div className="card-info__size">{dimensions}</div>}


            {(materials.length > 0) && <div className="accordion">
                <div className="accordion__main-title">Материалы</div>
                {materials.map((material, index) => {
                    return <div key={index} className="accordion__item">
                        <input type="checkbox" className="accordion__input visually-hidden" name={`spoiler${index}`} id={`spoiler${index}`}/>
                        <label htmlFor={`spoiler${index}`} className="accordion__title">
                            <div className="accordion__circle" style={{backgroundImage: `url('${material.imageUrl}')`}}></div>
                            {material.title}
                        </label>
                        <div className="accordion__body">{material.description}</div>
                        <svg className="accordion__svg" width="16" height="10" viewBox="0 0 16 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0.998657 0.999026L6.48949 7.45706C7.28054 8.37369 8.86265 8.37369 9.6537 7.45706L15.1445 0.999025" stroke="black" strokeWidth="2" strokeMiterlimit="10" strokeLinejoin="round"/>
                        </svg>
                    </div>;
                })}
            </div>}
        </div>
    );
};

export default CardInfo;
