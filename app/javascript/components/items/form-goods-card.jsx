import React, {Component} from 'react';
import {connect} from 'react-redux';
import axios from "axios";
import {ActionCreator} from "../../reducers/basket/basket";
import {ActionCreator as ActionCreatorNotification} from "../../reducers/notification/notification";

class FormGoodsCard extends Component {
    constructor(props) {
        super(props);

        this.state = {
            fieldNumber: 1,
            selectedColor: null
        };

        this.onPrevHandler = this.onPrevHandler.bind(this);
        this.onNextHandler = this.onNextHandler.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }


    onPrevHandler(evt) {
        evt.preventDefault();
        const {fieldNumber} = this.state;
        let stepPrev = 1;

        if (fieldNumber <= 1) {
            stepPrev = 0;
        }

        this.setState((state) => ({
            fieldNumber: state.fieldNumber - stepPrev
        }));
    }

    onNextHandler(evt) {
        evt.preventDefault();
        this.setState((state) => ({
            fieldNumber: state.fieldNumber + 1
        }));
    }

    handleChange(evt) {
        evt.preventDefault();
        let newNumber = +evt.target.value;

        if (newNumber <= 1) {
            newNumber = 1;
        }

        this.setState({
            fieldNumber: newNumber
        });
    }

    onSubmit(evt) {
        evt.preventDefault();
        const {goodsPageInfo, setBasketItems, setNotification} = this.props;
        const {fieldNumber, selectedColor} = this.state;

        const csrfToken = document.querySelector('meta[name="csrf-token"]').content;

        axios({
            method: 'post',
            url: `/api/add_basket_good`,
            data: {
                good_id: goodsPageInfo.id,
                color_id: selectedColor,
                count: fieldNumber

            },
            headers: {
                'X-CSRF-TOKEN': csrfToken
            }
        })
            .then((res) => {
                setBasketItems(res.data);
                setNotification({
                    text: res.data.message,
                    status: res.data.status
                });
            })
            .catch((error) => {
                setNotification({
                    text: error.response.data.message,
                    status: error.response.data.status
                });
            })
    }

    changeColor(evt) {
        const {setCurrentMainImg, goodsPageInfo} = this.props;
        this.setState({ selectedColor: +evt.target.value });

        const {colors} = goodsPageInfo;

        const getColorById = (id, colors) => {
            return colors.filter((color) => color.id === id)[0];
        };

        const currentColor = getColorById(+evt.target.value, colors);

        setCurrentMainImg(currentColor);
    }

    componentDidMount() {
        const {goodsPageInfo, setCurrentMainImg} = this.props;
        const {colors} = goodsPageInfo;
        this.setState({ selectedColor: colors[0].id });
        setCurrentMainImg(colors[0]);
    }

    getNameColor(id, arrayColors) {
        if (id === null || arrayColors === null) {
            return null;
        }

        return arrayColors.filter((color) => color.id === id)[0].title;
    }

    render() {
        const {isShowTooltipe, openTooltipe, goodsPageInfo} = this.props;
        const {colors} = goodsPageInfo;
        const {fieldNumber, selectedColor} = this.state;

        return (
            <form onSubmit={this.onSubmit} className="add-basket">
                <div className="change-color">
                    <div className="change-color__wrap">
                        {!isShowTooltipe && <div className="change-color__title">Выберите цвет: {this.getNameColor(selectedColor, colors)}</div>}
                        <div
                            className="change-color__desc"
                            onClick={openTooltipe}
                        >
                            Нужен другой цвет?
                        </div>
                    </div>

                    <div className="change-color__radio radio-color">

                        {colors.map((color, index) => {
                            return (
                                <div className="radio-color__wrap" key={color.id}>
                                    <input
                                        id={color.id}
                                        className="visually-hidden radio-color__input"
                                        type="radio"
                                        name="colors"
                                        value={color.id}
                                        checked={selectedColor === color.id}
                                        onChange={(e) => this.changeColor(e)}
                                    />
                                    <label htmlFor={color.id} className="radio-color__label" style={{backgroundColor: color.color}}/>
                                </div>
                            );
                        })}
                    </div>
                </div>
                <div className="change-count">
                    <div className="change-count__title">Количество</div>
                    <div className="change-count__input input-count">
                        <button
                            className="input-count__btn"
                            onClick={this.onPrevHandler}
                        >
                            <svg width="15" height="2" viewBox="0 0 15 2" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect width="15" height="1.07143" fill="black"/>
                            </svg>
                        </button>
                        <input
                            value={+fieldNumber}
                            onChange={this.handleChange}
                            type="number"
                            className="input-count__field"
                            min="0"
                            step="1"
                        />
                        <button
                            className="input-count__btn"
                            onClick={this.onNextHandler}
                        >
                            <svg width="15" height="16" viewBox="0 0 15 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect y="7" width="15" height="1.07143" fill="black"/>
                                <rect x="8.03516" y="0.0351562" width="15" height="1.07143" transform="rotate(90 8.03516 0.0351562)" fill="black"/>
                            </svg>
                        </button>
                    </div>
                </div>
                <div className="add-basket__footer">
                    <div className="add-basket__desc">Добавьте товары в корзину и отправьте нам запрос стоимости</div>
                    <button className="add-basket__submit btn" type="submit">Добавить в корзину</button>
                </div>
            </form>
        );
    }
};

const mapDispatchToProps = (dispatch) => ({
    setBasketItems: (items) => dispatch(ActionCreator.setBasketItems(items)),
    setNotification: (options) => dispatch(ActionCreatorNotification.setNotification(options))
});

export default connect(null, mapDispatchToProps)(FormGoodsCard);
