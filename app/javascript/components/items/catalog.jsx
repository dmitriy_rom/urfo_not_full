import React, {Fragment} from 'react';
import {Link} from 'react-router-dom';
import AnimationBase from "./animation-base";

const Catalog = (props) => {
    const {data, catalogCategory, vertical = false} = props;

    return (
        <Fragment>
            <div className={`catalog${vertical ? ` catalog--vertical` : ``}`}>
                {catalogCategory && (
                    <AnimationBase classDiv="catalog__category">
                        {catalogCategory}
                    </AnimationBase>
                )}
            </div>
            <div className={`catalog${vertical ? ` catalog--vertical` : ``}`}>
                {data.map((good, index) => {
                    return <Link to={`/${good.model}/${good.slug}`}
                                 key={`good-${index}`}
                                 className="catalog__item"
                    >
                        <AnimationBase>
                            <div className="catalog__img">
                                <img src={good.imageUrl} alt={good.title}/>
                            </div>
                            <div className="catalog__title">{good.title}</div>
                            <div className="catalog__desc">{good.shortDescription}</div>
                        </AnimationBase>
                    </Link>;
                })}
            </div>
        </Fragment>
    );
};

export default Catalog;
