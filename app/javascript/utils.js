const INTERVAL_CHANGE_SLIDE_IN_MAIN_SLIDER = 6000;

const Urls = {
    SERIES: `series`,
    PROJECTS: `projects`,
    GOODS: `goods`,
    CONTACTS: `contacts`,
    ABOUT: `about`,
    CATALOG: `catalog`,
    SEARCH: `search`,
    PAGES: `pages`
};

const KeyCodes = {
    ESCAPE: 27,
};

export {
    INTERVAL_CHANGE_SLIDE_IN_MAIN_SLIDER,
    Urls,
    KeyCodes
};
