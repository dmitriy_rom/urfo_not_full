const initialState = {
    contacts: {},
    seo: {}
};

const ActionType = {
    GET_CONTACTS: `GET_CONTACTS`,
    GET_SEO: `GET_SEO`,
};

const ActionCreator = {
    getContacts: (contacts) => ({
        type: ActionType.GET_CONTACTS,
        payload: contacts
    }),
    getSeo: (seo) => ({
        type: ActionType.GET_SEO,
        payload: seo
    }),
};

const Operations = {
    getContacts: () => (dispatch, _getState, api) => {
        return api.get(`/api/get_contacts`)
            .then((response) => {
                dispatch(ActionCreator.getContacts(response.data));
            });
    },
    getSeo: () => (dispatch, _getState, api) => {
        return api.get(`/api/get_seo`)
            .then((response) => {
                dispatch(ActionCreator.getSeo(response.data));
            });
    },
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionType.GET_CONTACTS: return Object.assign({}, state, {
            contacts: action.payload
        });
        case ActionType.GET_SEO: return Object.assign({}, state, {
            seo: action.payload
        });
    }

    return state;
};

export {
    reducer,
    ActionCreator,
    ActionType,
    Operations
};
