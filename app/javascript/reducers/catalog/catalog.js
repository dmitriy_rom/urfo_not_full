import {getAdaptedGoods} from "../../api/adapter-goods";

const initialState = {
    goodsCatalog: []
};

const ActionType = {
    GET_GOODS_CATALOG: `GET_GOODS_CATALOG`,
    SET_GOODS_CATALOG: `SET_GOODS_CATALOG`
};

const ActionCreator = {
    setGoodsCatalog: (goods) => ({
        type: ActionType.SET_GOODS_CATALOG,
        payload: goods
    })
};

const Operations = {
    getGoodsCatalog: () => (dispatch, _getState, api) => {
        return api.get(`/api/get_catalog_items`)
            .then((response) => {
                dispatch(ActionCreator.setGoodsCatalog(response.data));
            });
    }
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case ActionType.SET_GOODS_CATALOG: return Object.assign({}, state, {
            goodsCatalog: action.payload
        });
    }

    return state;
};

export {
    reducer,
    ActionCreator,
    ActionType,
    Operations
}
