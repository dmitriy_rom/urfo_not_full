ActiveAdmin.register_page "Dashboard" do
  menu priority: 1, label: proc { I18n.t("active_admin.dashboard") }

  content title: proc { I18n.t("active_admin.dashboard") } do
    div class: "blank_slate_container", id: "dashboard_default_message" do
      span class: "blank_slate" do
        "Добро пожаловать в панель администрирования сайта компании «URFO»."
      end
      div do
        ( "Сайт разработан в 2020 году компанией " +
            link_to("proektmarketing", "http://proektmarketing.ru", target: '_blank') +
            ". По вопросам использования системы или её доработки вы можете обращаться в службу тех. поддержки " +
            link_to("web@proektmarketing.ru", 'mailto:web@proektmarketing.ru', target: '_blank') +
            ".").html_safe
      end
      small "Сайт написан на языке Ruby 2.4.1 с применением веб-фреймворка Rails 5.2.4 и JavaScript-библиотеки React."
    end

    # Here is an example of a simple dashboard with columns and panels.
    #
    # columns do
    #   column do
    #     panel "Recent Posts" do
    #       ul do
    #         Post.recent(5).map do |post|
    #           li link_to(post.title, admin_post_path(post))
    #         end
    #       end
    #     end
    #   end

    #   column do
    #     panel "Info" do
    #       para "Welcome to ActiveAdmin."
    #     end
    #   end
    # end
  end # content
end
