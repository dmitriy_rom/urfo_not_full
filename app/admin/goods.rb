ActiveAdmin.register Good do
  permit_params :title, :slug, :height, :width, :length, :description, :short_description, :prior, :hided,
                :series_id, :seo_title, :seo_description, :seo_keywords, :group,
                project_ids: [], material_ids: [],
                good_colors_attributes: [:id, :color_id, :good_id, :good_image, :good_image_cache, :good_image_cache, :prior, :_destroy]

  filter :title
  filter :slug
  filter :height
  filter :length
  filter :width
  filter :series
  filter :projects
  filter :materials
  filter :prior
  filter :hided, collection: [ ["Да", true ],  ["Нет", false ] ]

  index do
    column :title
    column :slug
    column :short_description
    column :height
    column :width
    column :length
    column :prior
    column :hided
    column :series
    column "Изображение" do |good|
      image_tag(good.main_image.url, width: 100, height: 100)
    end
    actions
  end

  show do
    attributes_table do
      row :title
      row :slug
      row :short_description
      row :description
      row :height
      row :width
      row :length
      row "Изображения" do |good|
        ul do
          good.good_colors.each do |color|
            li image_tag(color.good_image.url, width: 200, height: 200)
          end
        end
      end
      row :prior
      row :hided
      row :series
      row :projects
      row :materials
      row :group do |good|
        if good.group == Good::MAIN_PRODUCT
          "Основная продукция"
        else
          "Другие товары"
        end
      end
      panel "SEO" do
        attributes_table_for good do
          row :seo_title
          row :seo_description
          row :seo_keywords
        end
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :slug, hint: I18n.t('active_admin.hints.slug').html_safe
      f.input :short_description, hint: I18n.t('active_admin.hints.short_description')
      f.input :description, hint: I18n.t('active_admin.hints.description')
      f.input :height, hint: "В миллиметрах"
      f.input :width, hint: "В миллиметрах"
      f.input :length, hint: "В миллиметрах"
      f.input :prior, hint: I18n.t('active_admin.hints.prior')
      f.input :hided
      f.input :series, hint: "Серия, которой принадлежит данный товар"
      f.input :projects, hint: "Проекты, в которых использовался данный товар"
      f.input :materials
      f.input :group, as: :radio, collection: { "Основная продукция" => 0, "Другие товары" => 1 }
      f.inputs "seo-параметры" do
        f.input :seo_title
        f.input :seo_description
        f.input :seo_keywords
      end
      f.inputs "Цвета товара" do
        f.has_many :good_colors, { heading: nil, allow_destroy: true, new_record: "Добавить цвет" } do |good_color|
          good_color.input :color, collection: Color.all.map { |color| [ color.title, color.id, {'data-color': color.color_code} ]}
          good_color.input :good_image, hint: ["<br>Желательно загружать изображение размером не менее 1300х1120",
              good_color.object.good_image.present? ?
                  "<br>Текущее изображение:<br>#{image_tag(good_color.object.good_image.url, width: 200, height: 200)}" : ""].join.html_safe
          good_color.input :good_image_cache, as: :hidden
          good_color.input :prior, hint: I18n.t('active_admin.hints.prior') + ". Самый приоритетный цвет будет использоваться в качестве изображения по умолчанию"
        end
      end
    end
    f.actions
  end
end