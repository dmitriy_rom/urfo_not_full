ActiveAdmin.register Order do

  permit_params :processed

  filter :name
  filter :email
  filter :phone
  filter :processed
  filter :created_at

  actions :index, :edit, :update

  index do
    column :name
    column :email
    column :phone
    column :processed
    column :created_at
    column :order_goods do |order|
      ul do
        order.order_goods.each do |order_good|
          li "#{order_good.good.title}, #{order_good.good_color.color.title}, #{order_good.count} шт"
        end
      end
    end
    actions
  end

  form do |f|
    f.inputs do
      f.input :processed, hint: "Заказ от #{f.object.created_at}, клиент: #{f.object.name}, email клиента: #{f.object.email}"
    end
    f.actions
  end
end