ActiveAdmin.register MainSlider do
  permit_params :project_id, :prior

  config.filters = false

  index do
    column :project
    column :prior
    actions
  end

  show do
    attributes_table do
      row :project
      row :prior
    end
  end

  form do |f|
    f.inputs do
      f.input :project, hint: "Подпись и изображение слайда будут браться у связанного с ним проекта"
      f.input :prior, hint: I18n.t('active_admin.hints.prior')
    end
    f.actions
  end
end