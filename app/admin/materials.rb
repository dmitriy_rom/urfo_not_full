ActiveAdmin.register Material do
  permit_params :title, :description, :image, :image_cache, :title, :prior

  filter :title

  index do
    column :title
    column :description
    column :image do |material|
      image_tag material.image.url, width: 100, height: 100
    end
    column :prior
    actions
  end

  show do
    attributes_table do
      row :title
      row :description
      row :image do |material|
        image_tag material.image.url, width: 100, height: 100
      end
      row :prior
    end
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :description
      f.input :image, hint: [f.object.image? ? "<br>Текущее изображение:<br>#{image_tag(f.object.image.url, width: 100, height: 100)}" : ""].join.html_safe
      f.input :image_cache, as: :hidden
      f.input :prior, hint: I18n.t('active_admin.hints.prior')
    end
    f.actions
  end
end