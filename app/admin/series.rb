ActiveAdmin.register Series do
  permit_params :title, :slug, :description, :preview_image, :preview_image_cache, :short_description, :group,
                :main_image, :main_image_cache, :prior, :hided, :seo_title, :seo_description, :seo_keywords, good_ids: []

  filter :title
  filter :slug
  filter :goods
  filter :prior
  filter :hided, collection: [ ["Да", true ],  ["Нет", false ] ]

  index do
    column :title
    column :slug
    column :short_description
    column :description
    column :goods do |series|
      ul do
        series.goods.each do |good|
          li good.title
        end
      end
    end
    column :preview_image do |series|
      image_tag series.preview_image.url, width: 200, height: 200
    end
    column :main_image do |series|
      image_tag series.main_image.url, width: 200, height: 200
    end
    column :prior do |series|
      "#{series.prior}" + "#{series.get_prior_info}"
    end
    column :hided
    actions
  end

  show do
    attributes_table do
      row :title
      row :slug
      row :short_description
      row :description
      row :preview_image do |series|
        image_tag series.preview_image.url
      end
      row :main_image do |series|
        image_tag series.main_image.url
      end
      row :goods do |series|
        ul do
          series.goods.each do |good|
            li good.title
          end
        end
      end
      row :group do |series|
        if series.group == Series::MAIN_PRODUCT
          "Основная продукция"
        else
          "Другие товары"
        end
      end
      panel "SEO" do
        attributes_table_for series do
          row :seo_title
          row :seo_description
          row :seo_keywords
        end
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :slug, hint: I18n.t('active_admin.hints.slug').html_safe
      f.input :short_description, hint: "Будет использоваться в каталоге"
      f.input :description, hint: "Будет использоваться на странице серии"
      f.input :preview_image, hint: ["Будет выводиться в каталоге, а также на главной странице для 3, 4, 5 серий, отсортированных по приоритету.",
                                     "<br>Желательно загружать изображение размером не менее 500x500" +
                                    (f.object.preview_image.present? ? "<br>Текущее изображение:<br>#{image_tag(f.object.preview_image.url, width: 200, height: 200)}" : "")].join.html_safe
      f.input :preview_image_cache, as: :hidden
      f.input :main_image, hint: [ "Будет выводиться на странице серии, а также на главной странице для двух наиболее приоритетных серий с вертикальными изображениями.",
                                   "<br>Желательно загружать изображение размером не менее 1322x721 для горизонтальных изображений и 830x1157 для вертикальных изображений" +
                                 (f.object.main_image.present? ? "<br>Текущее изображение:<br>#{image_tag(f.object.main_image.url, width: 200, height: 200)}" : "")].join.html_safe
      f.input :main_image_cache, as: :hidden
      f.input :goods, hint: "Товары, принадлежащие этой серии"
      f.input :prior, hint: [I18n.t('active_admin.hints.prior') +
                      "<br> #{Series::BIG_IMAGES_ON_MAIN + Series::SMALL_IMAGES_ON_MAIN} наиболее приоритетных серий будет выводиться на главной странице"].join.html_safe
      f.input :hided
      f.input :group, as: :radio, collection: { "Основная продукция" => 0, "Другие товары" => 1 }
      f.inputs "seo-параметры" do
        f.input :seo_title
        f.input :seo_description
        f.input :seo_keywords
      end
    end
    f.actions
  end
end
