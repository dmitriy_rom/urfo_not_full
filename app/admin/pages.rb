ActiveAdmin.register Page do
  permit_params :title, :slug,
                :seo_title, :seo_description, :seo_keywords

  action_item :vitrage, only: [:show, :edit] do
    link_to "Витраж", edit_page_path(resource.slug)
  end

  filter :title
  filter :slug

  index do
    column :title
    column :slug
    actions
  end

  show do
    attributes_table do
      row :title
      row :slug
      row :seo_title
      row :seo_description
      row :seo_keywords
    end
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :slug, hint: I18n.t('active_admin.hints.slug').html_safe
      f.inputs "seo-параметры" do
        f.input :seo_title
        f.input :seo_description
        f.input :seo_keywords
      end
    end
    f.actions
  end
end