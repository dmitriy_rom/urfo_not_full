ActiveAdmin.register Color do
  permit_params :title, :color_code

  filter :title

  index do
    column :title
    column :color_code do |color|
      span(style: "color: #{color.color_code}") do
        color.color_code
      end
    end
    actions
  end

  show do
    attributes_table do
      row :title
      row :color_code do |color|
        span(style: "color: #{color.color_code}") do
          color.color_code
        end
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :color_code
    end
    f.actions
  end
end