ActiveAdmin.register Setting do
  permit_params :value

  ### Disable some actions
  actions :all, except: [:new, :create, :show, :destroy]

  ### Setting up the menu element of this page
  menu priority: 19, parent: "Служебное"

  ## INDEX

  ### Index page Configuration
  config.filters = false
  config.batch_actions = false
  config.paginate = false
  config.sort_order = 'id_asc'

  ### Override `scoped_collection` for another or extended index collection
  controller do
    def scoped_collection
      super.visibles
    end
  end

  ### Index as table
  index do
    column :name, sortable: :name do |sset|
      [ sset.name.to_s,
        "<br>",
        "<small style=\"font-size:80%;color:grey;\">",
        sset.descr.to_s,
        "</small>" ].join.html_safe
    end
    column (:value) do |sset|
      sset.humanized_value
    end
    actions
  end

  ## FORM

  form html: { multipart: true } do |f|
    f.inputs f.object.name do
      case f.object.vtype
        when Setting::VTYPE_BOOLEAN
          f.input :value, as: :boolean, label: "#{f.object.name}?"
        when Setting::VTYPE_TEXT
          f.input :value, as: :text
        else
          f.input :value
      end
    end
    f.actions
  end

end
