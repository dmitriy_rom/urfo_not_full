ActiveAdmin.register Project do
  permit_params :title, :slug, :description, :preview_image, :preview_image_cache, :prior, :hided,
                :location_description, :main_image,
                :seo_title, :seo_description, :seo_keywords, good_ids: []

  action_item :vitrage, only: [:show, :edit] do
    link_to "Витраж", edit_project_path(resource.slug)
  end

  filter :title
  filter :slug
  filter :goods
  filter :prior
  filter :hided, collection: [ ["Да", true ],  ["Нет", false ] ]

  index do
    column :title
    column :slug
    column :description
    column :goods do |project|
      ul do
        project.goods.each do |good|
          li good.title
        end
      end
    end
    column :preview_image do |project|
      image_tag project.preview_image.url, width: 200, height: 200
    end
    column :location_description
    column :prior
    column :hided
    actions
  end

  show do
    attributes_table do
      row :title
      row :slug
      row :description
      row :preview_image do |project|
        image_tag project.preview_image.url
      end
      row :location_description
      row :goods do |project|
        ul do
          project.goods.each do |good|
            li good.title
          end
        end
      end
      row :prior
      row :hided
      panel "SEO" do
        attributes_table_for project do
          row :seo_title
          row :seo_description
          row :seo_keywords
        end
      end
    end
  end

  form do |f|
    f.inputs do
      f.input :title
      f.input :slug, hint: I18n.t('active_admin.hints.slug').html_safe
      f.input :description
      f.input :preview_image, hint: ["<br>Желательно загружать изображение размером не менее 2000x1150",
        f.object.preview_image.present? ? "<br>Текущее изображение:<br>#{image_tag(f.object.preview_image.url, width: 200, height: 200)}" : ""].join.html_safe
      f.input :preview_image_cache, as: :hidden
      f.input :location_description
      f.input :prior, hint: I18n.t('active_admin.hints.prior')
      f.input :hided
      f.input :goods, hint: "Товары, которые использовались в данном проекте"
      f.inputs "seo-параметры" do
        f.input :seo_title
        f.input :seo_description
        f.input :seo_keywords
      end
    end
    f.actions
  end
end