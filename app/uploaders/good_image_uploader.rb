class GoodImageUploader < BaseUploader
  version :main_thumb do
    process resize_to_fill: [1300, 1120]
  end

  version :catalog_thumb do
    process resize_to_fill: [500, 500]
  end

  version :series_page_thumb do
    process resize_to_fill: [340, 340]
  end

  version :basket_thumb do
    process resize_to_fill: [220, 220]
  end
end