class SeriesCatalogImageUploader < BaseUploader
  version :next_thumb do
    process resize_to_fill: [152, 152]
  end

  version :catalog_thumb do
    process resize_to_fill: [500, 500]
  end
end