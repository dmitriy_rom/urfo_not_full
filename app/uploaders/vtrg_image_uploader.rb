class VtrgImageUploader < BaseUploader
  version :thumb do
    process resize_to_fill: [200, 200]
  end

  version :two_images do
    process resize_to_fill: [830, 468]
  end

  version :three_images do
    process resize_to_fill: [500, 300]
  end

  version :big do
    process resize_to_fill: [1937, 1092]
  end

end
