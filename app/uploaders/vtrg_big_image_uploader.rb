class VtrgBigImageUploader < BaseUploader
  version :thumb do
    process resize_to_fill: [200, 200]
  end

  version :big do
    process resize_to_fill: [1937, 1092]
  end
end