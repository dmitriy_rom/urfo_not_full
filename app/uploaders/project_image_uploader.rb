class ProjectImageUploader < BaseUploader
  version :slider_thumb do
    process resize_to_fill: [2000, 1150]
  end

  version :catalog_thumb do
    process resize_to_fill: [954, 634]
  end

  version :next_thumb do
    process resize_to_fill: [152, 152]
  end
end