class SeriesImageUploader < BaseUploader
  version :main_vertical_thumb do
    process resize_to_fill: [707, 982]
  end

  version :show_vertical_thumb do
    process resize_to_fill: [830, 1157]
  end

  version :show_gorizontal_thumb do
    process resize_to_fill: [1322, 721]
  end
end