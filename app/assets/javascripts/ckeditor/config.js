if (typeof(CKEDITOR) != 'undefined') {
    CKEDITOR.editorConfig = function( config )
    {
        config.toolbar = 'vitrageToolbar';

        config.toolbar_vitrageToolbar =
            [
                { name: 'styles', items: [ 'Format' ] },
                { name: 'paragraph', items : [ 'BulletedList', 'NumberedList'] },

            ];

        configtoolbar = 'tableToolbar';

        config.toolbar_tableToolbar =
            [
                { name: 'insert', items: [ 'Table' ] },
                { name: 'styles', items: [ 'Format' ] },
            ];
        config.height = '143px';
        config.width = '100%';
        config.enterMode = CKEDITOR.ENTER_BR;
        config.shiftEnterMode = CKEDITOR.ENTER_BR;
    }
}