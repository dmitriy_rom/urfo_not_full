document.addEventListener('DOMContentLoaded', function(event){
    if(document.querySelector('.vtrg-add-new-wrapper')) {
        initRemoveElement();
        document.addEventListener('click', function(ev) {
            if(ev.target.classList.contains('icon--ei-pencil') || ev.target.classList.contains('vtrg-table')) {
                setTimeout(setStyleLink, 1000);
            }
        });
    }
    if(document.querySelector('.swiper-container')) {
        initSwipers();
    }
});

function createStyleLink() {
    let link = document.createElement('link');
    link.rel = "stylesheet";
    link.media = "all";
    link.href="/assets/vitrage_forms.self-d967c91739aa5cd16b3e4250f456e5ee50f83fc31d2174045adaa0bada334775.css?body=1";
    return link;
}

function setStyleLink() {
    if(document.querySelector('iframe')) {
        let link = createStyleLink(),
            iframes = document.querySelectorAll('iframe');
        for(let i = 0; i < iframes.length; i++) {
            iframes[i].contentDocument.head.appendChild(link);
        }
    }
}

function initRemoveElement() {
    document.addEventListener("click", function(event) {
        if(event.target.classList.contains('remove_link')) {
            event.preventDefault();
            removeElement(event.target, '.gallery-image-nested-block');
        }
    });
}

function removeElement(target, selector) {
    var $imageForm = target.closest(selector);
    $imageForm.parentNode.removeChild($imageForm);
}

function initSwipers() {
    let swiper = new Swiper('.js-swiper', {
        loop: true,
        navigation: {
            nextEl: '.btn-next',
            prevEl: '.btn-prev',
        }
    });
    while(document.querySelectorAll('.js-swiper').length) {
        document.querySelectorAll('.js-swiper')[document.querySelectorAll('.js-swiper').length - 1].classList.remove('js-swiper');
    }
}

function createErrorMessage(val) {
    let errorBlock = document.createElement('div');
    errorBlock.classList.add('error-message');
    errorBlock.innerHTML = val;
    return errorBlock;
}