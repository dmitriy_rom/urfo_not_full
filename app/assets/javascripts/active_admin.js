//= require arctic_admin/base
//= require activeadmin_addons/all

document.addEventListener('DOMContentLoaded', function(event) {
    // check good colors container presence
    if(document.querySelector('.has_many_container.good_colors')) {
        document.querySelector('body').addEventListener('click', function(ev) {
            // check if click target is a select
            if(ev.target.classList.contains('select2-selection') || ev.target.classList.contains('select2-selection__rendered')) {
                setColors(ev.target);
            }
        });
    }
});

function setColors(target) {
    // check if select container is open
    if(document.querySelector('.select2-container--open')) {
        // check if colors select options are present
        if(document.querySelector('ul[id*=select2-good_good_colors_attributes_]')) {
            let customOptionsContainer = document.querySelector('ul.select2-results__options');
            let customOptionIndex = 0;
            for(let i = 0; i < document.querySelector('select[id*=good_good_colors_attributes_]').options.length; i++) {
                // get option from default container
                let option = document.querySelector('select[id*=good_good_colors_attributes_]').options[i];
                if(option.dataset.color) {
                    let colorCircle = createColorCircle(option.dataset.color);
                    try {
                        customOptionsContainer.querySelectorAll('li')[customOptionIndex].appendChild(colorCircle);
                        customOptionIndex++;
                    } catch(error) {
                        console.log("Ошибка " + error.name + ": " + error.message);
                    }
                }
            }
        }
    }
}

function createColorCircle(color) {
    let span = document.createElement('span');
    span.classList.add('colorCircle');
    span.style.backgroundColor = color;
    return span;
}