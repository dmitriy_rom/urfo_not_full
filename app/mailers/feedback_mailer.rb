class FeedbackMailer < ApplicationMailer
  default template_path: 'mailers'

  def send_connect_email(name, email, phone)
    @name = name
    @email = email
    @phone = phone
    mail to: (Setting.find_by(ident: "feedback_email").value),
         subject: "Запрос обратной связи с сайта URFO"
  end

  def send_order_email(order)
    @order = order
    mail to: (Setting.find_by(ident: "feedback_email").value),
         subject: "Запрос стоимости заказа с сайта URFO"
  end

  def send_error_email(message)
    @message = message
    mail_receiver = Setting.find_by(ident: "error_email").present? ? Setting.find_by(ident: "error_email").value : "web@proektmarketing.ru"
    mail to: mail_receiver, subject: "Произошла ошибка на сайте URFO"
  end
end
