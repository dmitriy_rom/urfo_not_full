class Project < ApplicationRecord
  has_one :main_slider, dependent: :destroy
  validates :title, :preview_image, :slug, :prior, presence: true
  validates :title, :slug, uniqueness: true
  mount_uploader :preview_image, ProjectImageUploader
  has_and_belongs_to_many :goods

  acts_as_vitrage_owner

  before_validation :prepare_slug

  scope :ordered, -> { order(prior: :asc, id: :desc) }
  scope :visibles, -> { where(hided: false) }

  def get_styled_title
    styled_title = []
    self.title.split(' ').each do |word|
      final_word = word
      self.get_related_series.pluck(:title).each do |series_title|
        if word.index(series_title)
          final_word = "<span class='accent-project-word'>#{word}</span>"
          break
        end
      end
      styled_title.push(final_word)
    end
    return styled_title.join(' ')
  end

  def get_related_series
    Series.joins(goods: [:goods_projects]).where(goods_projects: { project_id: self.id  }).distinct
  end

  def prepare_slug
    self.slug = UrlStringPreparator.slug(self.slug, self.title) unless self.slug.present?
  end

  def next
    if Project.visibles.count > 1
      Project.visibles.where("id > ?", id).order("id ASC").first || Project.visibles.first
    else
      nil
    end
  end
end
