class GoodColor < ApplicationRecord
  validates :good_image, :prior, presence: true
  validates :good, uniqueness: { scope: :color }
  belongs_to :color
  belongs_to :good
  has_many :basket_goods

  mount_uploader :good_image, GoodImageUploader

  scope :ordered, -> { order(prior: :asc, id: :desc) }
end
