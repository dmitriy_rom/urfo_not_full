class BasketGood < ApplicationRecord
  validates :count, :good, :good_color, :basket, presence: true
  validates :good, uniqueness: { scope: [:good_color, :basket] }
  belongs_to :basket
  belongs_to :good
  belongs_to :good_color
end
