class Basket < ApplicationRecord
  validates :client_makeshift_id, presence: true
  validates :client_makeshift_id, uniqueness: true
  has_many :basket_goods
end
