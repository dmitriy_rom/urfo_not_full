class Page < ApplicationRecord
  validates :title, :slug, presence: true
  validates :title, :slug, uniqueness: true

  acts_as_vitrage_owner

  before_validation :prepare_slug

  def prepare_slug
    self.slug = UrlStringPreparator.slug(self.slug, self.title) unless self.slug.present?
  end
end
