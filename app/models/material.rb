class Material < ApplicationRecord
  validates :title, :description, :image, :prior, presence: true
  mount_uploader :image, MaterialImageUploader
  has_and_belongs_to_many :goods

  scope :ordered, -> { order(prior: :asc, id: :desc) }
end
