class Setting < ApplicationRecord
  # Value types constants
  VTYPE_STRING    = 0
  VTYPE_BOOLEAN   = 1
  VTYPE_NUMBER    = 2
  VTYPE_DATETIME  = 3
  VTYPE_TEXT      = 4
  #VTYPE_FILE      = 5
  #VTYPE_MAP_POINT = 6
  #VTYPE_PAGE      = 7
  #VTYPE_RICH_TEXT = 8

  validates :ident, :name, presence: true

  scope :visibles, -> { where(hidden: false) }

  def humanized_value
    current_value = self.value
    case self.vtype
      when VTYPE_BOOLEAN
        current_value ? I18n.t('yep') : I18n.t('nope')
      else
        current_value.to_s
    end
  end
end
