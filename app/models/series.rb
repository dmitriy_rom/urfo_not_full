class Series < ApplicationRecord
  require 'mini_magick'

  MAIN_PRODUCT = 0
  OTHER_PRODUCT = 1

  BIG_IMAGES_ON_MAIN = 2
  SMALL_IMAGES_ON_MAIN = 3
  
  validates :title, :slug, :short_description, :prior, :preview_image, :main_image, presence: true
  validates :title, :slug, uniqueness: true
  mount_uploader :preview_image, SeriesCatalogImageUploader
  mount_uploader :main_image, SeriesImageUploader
  has_many :goods

  before_save :set_vertical_flag
  before_validation :prepare_slug

  scope :ordered, -> { order(prior: :asc, id: :desc) }
  scope :visibles, -> { where(hided: false) }

  def set_vertical_flag
    if self.present? && self.main_image.present?
      image = MiniMagick::Image.open(self.main_image.path)
      image[:height] > image[:width] ? self.vertical = true : self.vertical = false
    end
  end

  def prepare_slug
    if self.slug.blank?
      self.slug = UrlStringPreparator.slug(self.slug, self.title)
    end
  end

  def get_prior_info
    if Series.ordered.where(vertical: true).limit(2).pluck(:id).include?(self.id)
      return ", большое изображение выводится на главной"
    end
    if Series.ordered.where.not(id: Series.ordered.where(vertical: true).limit(2)).limit(3).pluck(:id).include?(self.id)
      return ", маленькое изображение выводится на главной"
    end
    return ""
  end

  def next
    if Series.visibles.count > 1
      Series.visibles.where("id > ?", id).order("id ASC").first || Series.visibles.first
    else
      nil
    end
  end

end
