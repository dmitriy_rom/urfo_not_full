class VitrageOwnersPiecesSlot < ActiveRecord::Base
  # stored fields: :owner_type, :owner_id, :piece_type, :piece_id, :ordn

  belongs_to :owner, polymorphic: true
  belongs_to :piece, polymorphic: true, dependent: :destroy

  after_create :set_ordn

  default_scope -> { order(ordn: :asc, id: :asc) }

  def set_ordn
    self.update(ordn: self.id)
  end

  PIECE_CLASSES_STRINGS = %w(VtrgBigImage VtrgTwoImages VtrgThreeImages VtrgSlider VtrgEditor VtrgTable VtrgText) # add pieces class names strings here (demodulized)
end
