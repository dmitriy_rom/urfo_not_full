module VitragePieces
  class VtrgThreeImages < ApplicationRecord
    has_one :slot, class_name: "VitrageOwnersPiecesSlot", as: :piece

    validate :one_image_presence

    mount_uploader :first_image, VtrgImageUploader
    mount_uploader :second_image, VtrgImageUploader
    mount_uploader :third_image, VtrgImageUploader

    def one_image_presence
      if self.first_image.blank? && self.second_image.blank? && self.third_image.blank?
        errors.add(:first_image, I18n.t('errors.vitrage.vtrg_images_blank'))
      end
    end

    def params_for_permit
      [:first_image, :second_image, :third_image, :first_image_title, :second_image_title, :third_image_title]
    end

  end
end
