module VitragePieces
  class VtrgTable < ApplicationRecord
    has_one :slot, class_name: "VitrageOwnersPiecesSlot", as: :piece

    validates :body, presence: true

    def params_for_permit
      [:body]
    end

  end
end
