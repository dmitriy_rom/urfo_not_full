module VitragePieces
  class VtrgSlider < ApplicationRecord
    has_one :slot, class_name: "VitrageOwnersPiecesSlot", as: :piece

    has_many :vtrg_slider_images, dependent: :delete_all

    accepts_nested_attributes_for :vtrg_slider_images, allow_destroy: true, reject_if: :all_blank
    validates_associated :vtrg_slider_images

    def params_for_permit
      [:vtrg_slider_images, vtrg_slider_images_attributes: [ :id, :image, :image_cache, :prior, :title, :_destroy ]]
    end

  end
end
