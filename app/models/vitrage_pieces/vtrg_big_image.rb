module VitragePieces
  class VtrgBigImage < ApplicationRecord
    has_one :slot, class_name: "VitrageOwnersPiecesSlot", as: :piece

    validates :image, presence: true

    mount_uploader :image, VtrgBigImageUploader

    def params_for_permit
      [:image, :image_title]
    end

  end
end
