class MainSlider < ApplicationRecord
  validates :prior, presence: true
  validates :project, uniqueness: true
  belongs_to :project

  scope :ordered, -> { order(prior: :asc, id: :desc) }
end
