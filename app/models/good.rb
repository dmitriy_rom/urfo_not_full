class Good < ApplicationRecord

  MAIN_PRODUCT = 0
  OTHER_PRODUCT = 1

  validates :title, :slug, presence: true
  validates :title, :slug, uniqueness: true
  validate :good_color_presence
  # remove required relation to series with "optional" flag
  belongs_to :series, optional: true
  has_and_belongs_to_many :projects
  has_and_belongs_to_many :materials
  has_many :good_colors
  has_many :basket_goods

  scope :visibles, -> { where(hided: false) }
  scope :ordered, -> { order(prior: :asc, id: :desc) }

  accepts_nested_attributes_for :good_colors, allow_destroy: true

  before_validation :prepare_slug

  def prepare_slug
    self.slug = UrlStringPreparator.slug(self.slug, self.title) unless self.slug.present?
  end

  def good_color_presence
    if self.good_colors.empty?
      errors.add(:title, I18n.t('errors.good.good_colors_empty'))
    end
  end

  def main_image
    self.good_colors.ordered.first.good_image
  end

end
