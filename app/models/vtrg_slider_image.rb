class VtrgSliderImage < ApplicationRecord
  belongs_to :vrtg_slider, optional: true

  mount_uploader :image, VtrgSliderImageUploader

  validates :image, :prior, presence: true

  default_scope { order(prior: :asc, id: :asc) }
end
