class OrderGood < ApplicationRecord
  validates :count, :good, :good_color, :order, presence: true
  validates :good, uniqueness: { scope: [:good_color, :order] }
  belongs_to :order
  belongs_to :good
  belongs_to :good_color
end
