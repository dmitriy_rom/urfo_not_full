class Color < ApplicationRecord
  validates :title, :color_code, presence: true
  validates :title, :color_code, uniqueness: true
  has_many :good_colors
end
