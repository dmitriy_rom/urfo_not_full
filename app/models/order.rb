class Order < ApplicationRecord
  validates :name, :email, presence: true
  has_many :order_goods, dependent: :destroy

  validate :good_presence

  def good_presence
    if self.order_goods.empty?
      errors.add(:title, I18n.t('errors.order.order_goods_empty'))
    end
  end
end
