class ProjectsController < ApplicationController
  before_action :authenticate_admin_user!, only: :edit

  def edit
    @project = Project.find_by(slug: params[:slug])
  end

end
