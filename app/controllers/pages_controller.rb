class PagesController < ApplicationController
  before_action :authenticate_admin_user!, only: :edit

  def edit
    @page = Page.find_by(slug: params[:slug])
  end
end
