class ApplicationController < ActionController::Base

  def get_basket
    if cookies[:client_makeshift_id].present? && Basket.find_by(client_makeshift_id: cookies[:client_makeshift_id]).present?
      return Basket.find_by(client_makeshift_id: cookies[:client_makeshift_id])
    else
      client_makeshift_id = generate_client_makeshift_id
      cookies[:client_makeshift_id] = client_makeshift_id
      return Basket.create(client_makeshift_id: client_makeshift_id)
    end
  end

  def generate_client_makeshift_id
    loop do
      token = SecureRandom.hex(10)
      break token unless Basket.where(client_makeshift_id: token).exists?
    end
  end

  def get_basket_goods(basket)
    goods = []
    basket.basket_goods.each do |basket_good|
      goods.push({ id: basket_good.id, title: basket_good.good.title, colorTitle: basket_good.good_color.color.title,
                   image: basket_good.good_color.good_image.basket_thumb.url, colorCode: basket_good.good_color.color.color_code, count: basket_good.count})
    end
    return goods
  end

  def get_entity_seo(obj)
    seo = {  title: obj.seo_title.present? ? obj.seo_title : default_seo_title,
              description: obj.seo_description.present? ? obj.seo_description : default_seo_description,
              keywords: obj.seo_keywords.present? ? obj.seo_keywords : default_keywords
    }
    return seo
  end

  def default_seo_title
    return Setting.exists?(ident: "seo_title") ? Setting.find_by(ident: "seo_title").value : Seorel.config.default_title
  end

  def default_seo_description
    return Setting.exists?(ident: "seo_description") ? Setting.find_by(ident: "seo_description").value : Seorel.config.default_description
  end

  def default_keywords
    Setting.exists?(ident: "seo_keywords") ? Setting.find_by(ident: "seo_keywords").value : Seorel.config.default_keywords
  end
end
