class Api::OrdersController < ApplicationController
  def create
    begin
      basket = get_basket
      order = Order.new(order_params)
      basket.basket_goods.each do |basket_good|
        order.order_goods.push(OrderGood.new(count: basket_good.count, good: basket_good.good, good_color: basket_good.good_color))
      end
      if order.save
        basket.basket_goods.destroy_all
        FeedbackMailer.send_order_email(order).deliver_now
        goods = get_basket_goods(basket)
        return render json: { status: "success", goodsCount: basket.basket_goods.count, goods: goods }
      else
        return render json: { status: "error", message: order.errors.messages.first[1][0] }, status: :unprocessable_entity
      end
    rescue
      return render json: { status: "error", message: "Ошибка при создании заказа" }, status: :unprocessable_entity
    end
  end

  def order_params
    params.require(:order).permit(:name, :email, :phone)
  end
end