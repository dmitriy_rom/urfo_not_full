class Api::ProjectsController < ApplicationController
  def index
    projects = Project.visibles.ordered
    data = []
    projects.each do |project|
      data.push( { title: project.get_styled_title, imageUrl: project.preview_image.catalog_thumb.url,
                 description: project.location_description, slug: project.slug} )
    end
    return render json: data
  end

  def show
    @project = Project.find_by(slug: params[:slug])
    data = render_to_string("projects/_show", :formats => [:html], :layout => false, :locals => {:project => @project})
    return render json: data
  end

  def get_related_series
    project = Project.find_by(slug: params[:slug])
    related_series = project.get_related_series.visibles.ordered
    data = []
    related_series.each do |series|
      data.push({title: series.title, imageUrl: series.preview_image.catalog_thumb.url, shortDescription: series.short_description,
                 group: series.group, model: "series", slug: series.slug})
    end
    return render json: data
  end

  def get_project_seo
    project = Project.find_by(slug: params[:slug])
    data = get_entity_seo(project)
    return render json: data
  end

  def get_next_project
    project = Project.find_by(slug: params[:slug])
    next_project = project.next ? { title: project.next.title, url: "/projects/#{project.next.slug}", linkTitle: "Следующий кейс" } : {}
    return render json: next_project
  end

end
