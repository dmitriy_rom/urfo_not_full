class Api::BasketsController < ApplicationController
  def show
    basket = get_basket
    goods = get_basket_goods(basket)
    return render json: { status: "success", goodsCount: basket.basket_goods.count, goods: goods }, status: :ok
  end

  def add_basket_good
    if params[:good_id].present? && params[:color_id].present? && params[:count].present?
      if Good.exists?(params[:good_id]) && GoodColor.exists?(params[:color_id])
        basket = get_basket
        basket_good = BasketGood.new(basket: basket, good: Good.find(params[:good_id]),
                                     good_color: GoodColor.find(params[:color_id]), count: params[:count])
        if basket_good.save
          goods = get_basket_goods(basket)
          return render json: { status: "success", message: "Товар добавлен в корзину",
                                goodsCount: basket.basket_goods.count, goods: goods }, status: :ok
        else
          return render json: { status: "error", message: basket_good.errors.messages.first[1][0] }, status: :unprocessable_entity
        end
      else
        return render json: { status: "error", message: "Извините, произошла ошибка" }, status: :unprocessable_entity
      end
    else
      return render json: { status: "error", message: "Извините, произошла ошибка" }, status: :unprocessable_entity
    end
  end

  def remove_basket_good
    basket = get_basket
    if params[:id].present? && basket.basket_goods.exists?(params[:id])
      begin
        BasketGood.find(params[:id]).destroy
        goods = get_basket_goods(basket)
        return render json: { status: "success", goodsCount: basket.basket_goods.count, goods: goods }, status: :ok
      rescue
        return render json: { status: "error", message: "Произошла ошибка при удалении" }, status: :unprocessable_entity
      end
    else
      return render json: { status: "error", message: "Извините, произошла ошибка" }, status: :unprocessable_entity
    end
  end
end
