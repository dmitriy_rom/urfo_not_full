class Api::SeriesController < ApplicationController
  def show
    data = {}
    if params[:slug].present? && Series.find_by(slug: params[:slug]).present?
      series = Series.find_by(slug: params[:slug])
      next_series_data = series.next ? { title: series.next.title, imageUrl: series.next.preview_image.next_thumb.url,
                                         url: "/series/#{series.next.slug}", linkTitle: "Следующий проект"  } : nil
      data = { title: series.title, description: series.description, vertical: series.vertical,
               imageUrl: series.vertical ? series.main_image.show_vertical_thumb.url : series.main_image.show_gorizontal_thumb.url,
               materials: Material.joins(:goods).where(goods: { series_id: series.id }).ordered.distinct.map { |material|
                 { title: material.title, description: material.description, imageUrl: material.image.thumb.url } },
               goods: series.goods.visibles.ordered.map { |good| { title: good.title, imageUrl: good.main_image.series_page_thumb.url, slug: good.slug} },
               next_series: next_series_data,
               seo: get_entity_seo(series)
      }
    end
    return render json: data
  end

  def get_small_home_series
    series = Series.visibles.ordered.where.not(id: Series.visibles.ordered.where(vertical: true).limit(2)).limit(3)
    data = []
    series.each do |series|
      data.push({ title: series.title, shortDescription: series.short_description, imageUrl: series.preview_image.catalog_thumb.url,
                 slug: series.slug, model: "series" } )
    end
    return render json: data
  end

  def get_big_home_series
    series = Series.visibles.ordered.where(vertical: true).limit(2)
    data = []
    series.each do |series|
      data.push({ title: series.title, shortDescription: series.short_description, imageUrl: series.main_image.main_vertical_thumb.url,
                 slug: series.slug, model: "series" })
    end
    return render json: data
  end

  def get_related_projects
    data = []
    if params[:slug].present? && Series.find_by(slug: params[:slug]).present?
      series = Series.find_by(slug: params[:slug])
      projects = Project.visibles.ordered.joins(:goods).where(goods: { series_id: series.id }).distinct
      data = []
      projects.each do |project|
        data.push( { title: project.get_styled_title, imageUrl: project.preview_image.catalog_thumb.url,
                     description: project.location_description, slug: project.slug} )
      end
    end
    return render json: data
  end

end
