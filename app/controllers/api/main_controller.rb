class Api::MainController < ApplicationController
  def get_contacts
    data = { email: Setting.exists?(ident: "feedback_email") ? Setting.find_by(ident: "feedback_email").value : "",
             phone: Setting.exists?(ident: "feedback_phone") ? Setting.find_by(ident: "feedback_phone").value : "",
             city: Setting.exists?(ident: "city") ? Setting.find_by(ident: "city").value : "",
             address: Setting.exists?(ident: "address") ? Setting.find_by(ident: "address").value : "",
             instaLink: Setting.exists?(ident: "instagram_link") ? Setting.find_by(ident: "instagram_link").value : "",
             facebookLink: Setting.exists?(ident: "facebook_link") ? Setting.find_by(ident: "facebook_link").value : ""
    }
    return render json: data
  end

  def get_seo
    data = {  title: default_seo_title,
              description: default_seo_description,
              keywords: default_keywords
    }
    return render json: data
  end

  def get_instagram_media
    url = URI.parse("https://graph.instagram.com/#{ENV["instagram_id"]}/media?fields=media_url,media_type,permalink,thumbnail_url&access_token=#{ENV["instagram_token"]}")
    data = []
    begin
      response = JSON.parse(Net::HTTP.get(url))["data"].first(4)
      response.each do |file|
        data.push({ imageUrl: file["media_type"] == "VIDEO" ? file["thumbnail_url"] : file["media_url"],
                    postUrl: file["permalink"] })
      end
      return render json: { status: "success", data: data }, status: :ok
    rescue
      FeedbackMailer.send_error_email("Ошибка загрузки данных из Инстаграма").deliver_now
      return render json: { status: "error", message: "Ошибка загрузки данных из Инстаграма" }, status: :unprocessable_entity
    end
  end
end
