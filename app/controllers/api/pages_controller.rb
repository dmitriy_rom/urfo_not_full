class Api::PagesController < ApplicationController
  def show
    @page = Page.find_by(slug: params[:slug])
    data = render_to_string("pages/_show", :formats => [:html], :layout => false)
    return render json: data
  end

  def get_seo
    page = Page.find_by(slug: params[:slug])
    data = get_entity_seo(page)
    return render json: data
  end
end
