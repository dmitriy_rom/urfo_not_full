class Api::SlidersController < ApplicationController
  def index
    @sliders = MainSlider.all
    data = []
    @sliders.each_with_index do |slide, i|
      data.push({title: slide.project.get_styled_title, imageUrl: slide.project.preview_image.url, slideIndex: i + 1, slug: slide.project.slug} )
    end
    render json: data
  end
end
