class Api::FeedbacksController < ApplicationController

  def feedback_request
    if params[:name].present? && params[:email].present?
      begin
        FeedbackMailer.send_connect_email(params[:name], params[:email], params[:phone]).deliver_now
        return render json: {status: "success" }, status: :ok
      rescue
        return render json: { status: "error" }, status: :unprocessable_entity
      end
    else
      return render json: { status: "error", message: "Заполните имя и email" }, status: :unprocessable_entity
    end
  end

end