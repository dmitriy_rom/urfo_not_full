class Api::GoodsController < ApplicationController
  def show
    data = {}
    if params[:slug].present? && Good.find_by(slug: params[:slug]).present?
      good = Good.find_by(slug: params[:slug])
      if good.width && good.height && good.length
        dimensions = "#{good.width} x #{good.height} x #{good.length} мм"
      else
        dimensions = nil
      end
      data = { id: good.id, title: good.title, dimensions: dimensions, seriesSlug: good.series.present? ? good.series.slug : nil,
               materials: good.materials.ordered.map { |material| { title: material.title, description: material.description, imageUrl: material.image.thumb.url } },
               seo: get_entity_seo(good),
               colors: good.good_colors.ordered.map { |color|
                 { id: color.id, color: color.color.color_code, title: color.color.title, imageUrl: color.good_image.main_thumb.url } }
      }
    end
    return render json: data
  end
end
