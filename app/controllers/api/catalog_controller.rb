class Api::CatalogController < ApplicationController

  def index
    # Группы: 0 - основная продукция, 1 - другие товары
    data = []
    series = Series.visibles.ordered
    series.each do |series|
      data.push({title: series.title, imageUrl: series.preview_image.catalog_thumb.url, shortDescription: series.short_description,
                group: series.group, model: "series", slug: series.slug})
    end
    goods = Good.where(series_id: nil).visibles.ordered
    goods.each do |good|
      data.push({title: good.title, imageUrl: good.good_colors.ordered.first.good_image.catalog_thumb.url, shortDescription: good.short_description,
                group: good.group, model: "goods", slug: good.slug})
    end
    render json: data
  end

end
