# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_03_07_035306) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admin_users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_admin_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true
  end

  create_table "basket_goods", force: :cascade do |t|
    t.integer "count", default: 1, null: false
    t.bigint "good_id", null: false
    t.bigint "good_color_id", null: false
    t.bigint "basket_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["basket_id"], name: "index_basket_goods_on_basket_id"
    t.index ["good_color_id"], name: "index_basket_goods_on_good_color_id"
    t.index ["good_id", "good_color_id", "basket_id"], name: "index_basket_goods_on_good_id_and_good_color_id_and_basket_id", unique: true
    t.index ["good_id"], name: "index_basket_goods_on_good_id"
  end

  create_table "baskets", force: :cascade do |t|
    t.string "client_makeshift_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "ckeditor_assets", force: :cascade do |t|
    t.string "data_file_name", null: false
    t.string "data_content_type"
    t.integer "data_file_size"
    t.string "type", limit: 30
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["type"], name: "index_ckeditor_assets_on_type"
  end

  create_table "colors", force: :cascade do |t|
    t.string "title", null: false
    t.string "color_code", default: "#000000", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "good_colors", force: :cascade do |t|
    t.string "good_image", null: false
    t.integer "prior", default: 9, null: false
    t.bigint "color_id", null: false
    t.bigint "good_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["color_id"], name: "index_good_colors_on_color_id"
    t.index ["good_id", "color_id"], name: "index_good_colors_on_good_id_and_color_id", unique: true
    t.index ["good_id"], name: "index_good_colors_on_good_id"
  end

  create_table "goods", force: :cascade do |t|
    t.string "title", null: false
    t.string "slug", null: false
    t.integer "height"
    t.integer "width"
    t.integer "length"
    t.text "description"
    t.string "short_description", null: false
    t.integer "prior", default: 9, null: false
    t.boolean "hided", default: false
    t.integer "group", default: 0
    t.bigint "series_id"
    t.string "seo_title"
    t.string "seo_description"
    t.string "seo_keywords"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["series_id"], name: "index_goods_on_series_id"
  end

  create_table "goods_materials", id: false, force: :cascade do |t|
    t.bigint "good_id", null: false
    t.bigint "material_id", null: false
  end

  create_table "goods_projects", id: false, force: :cascade do |t|
    t.bigint "good_id", null: false
    t.bigint "project_id", null: false
  end

  create_table "main_sliders", force: :cascade do |t|
    t.integer "prior", default: 9, null: false
    t.bigint "project_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["project_id"], name: "index_main_sliders_on_project_id"
  end

  create_table "materials", force: :cascade do |t|
    t.string "title", null: false
    t.text "description", null: false
    t.string "image", null: false
    t.integer "prior", default: 9, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "order_goods", force: :cascade do |t|
    t.integer "count", default: 1, null: false
    t.bigint "good_id", null: false
    t.bigint "good_color_id", null: false
    t.bigint "order_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["good_color_id"], name: "index_order_goods_on_good_color_id"
    t.index ["good_id", "good_color_id", "order_id"], name: "index_order_goods_on_good_id_and_good_color_id_and_order_id", unique: true
    t.index ["good_id"], name: "index_order_goods_on_good_id"
    t.index ["order_id"], name: "index_order_goods_on_order_id"
  end

  create_table "orders", force: :cascade do |t|
    t.string "name", null: false
    t.string "email", null: false
    t.string "phone"
    t.boolean "processed", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "pages", force: :cascade do |t|
    t.string "title", null: false
    t.string "slug"
    t.string "seo_title"
    t.string "seo_description"
    t.string "seo_keywords"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "projects", force: :cascade do |t|
    t.string "title", null: false
    t.string "slug", null: false
    t.text "description"
    t.string "preview_image", null: false
    t.string "location_description"
    t.integer "prior", default: 9, null: false
    t.boolean "hided", default: false
    t.string "seo_title"
    t.string "seo_description"
    t.string "seo_keywords"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "seorel_seorels", id: :serial, force: :cascade do |t|
    t.string "title"
    t.string "description"
    t.string "image"
    t.string "seorelable_type"
    t.integer "seorelable_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["seorelable_id", "seorelable_type"], name: "index_seorel_seorels_on_seorelable_id_and_seorelable_type"
  end

  create_table "series", force: :cascade do |t|
    t.string "title", null: false
    t.string "slug", null: false
    t.text "description"
    t.string "preview_image", null: false
    t.string "main_image", null: false
    t.string "short_description", null: false
    t.integer "prior", default: 9, null: false
    t.boolean "hided", default: false
    t.boolean "vertical", default: false
    t.integer "group", default: 0
    t.string "seo_title"
    t.string "seo_description"
    t.string "seo_keywords"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "settings", force: :cascade do |t|
    t.string "ident", null: false
    t.string "name", null: false
    t.text "descr"
    t.integer "vtype", default: 0
    t.text "value"
    t.boolean "hidden", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "vitrage_owners_pieces_slots", id: :serial, force: :cascade do |t|
    t.string "owner_type", null: false
    t.integer "owner_id", null: false
    t.string "piece_type"
    t.integer "piece_id"
    t.integer "ordn", default: 9, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["ordn"], name: "index_vitrage_owners_pieces_slots_on_ordn"
    t.index ["owner_type", "owner_id"], name: "index_vitrage_owners_pieces_slots_on_owner_type_and_owner_id"
    t.index ["piece_type", "piece_id"], name: "index_vitrage_owners_pieces_slots_on_piece_type_and_piece_id"
  end

  create_table "vtrg_big_images", id: :serial, force: :cascade do |t|
    t.string "image"
    t.string "image_title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "vtrg_editors", id: :serial, force: :cascade do |t|
    t.text "body"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "vtrg_slider_images", force: :cascade do |t|
    t.string "image", null: false
    t.string "title"
    t.integer "prior", default: 9, null: false
    t.bigint "vtrg_slider_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["vtrg_slider_id"], name: "index_vtrg_slider_images_on_vtrg_slider_id"
  end

  create_table "vtrg_sliders", id: :serial, force: :cascade do |t|
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "vtrg_tables", id: :serial, force: :cascade do |t|
    t.text "body"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "vtrg_texts", id: :serial, force: :cascade do |t|
    t.text "body"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "vtrg_three_images", id: :serial, force: :cascade do |t|
    t.string "first_image"
    t.string "second_image"
    t.string "third_image"
    t.string "first_image_title"
    t.string "second_image_title"
    t.string "third_image_title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "vtrg_two_images", id: :serial, force: :cascade do |t|
    t.string "first_image"
    t.string "second_image"
    t.string "first_image_title"
    t.string "second_image_title"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
