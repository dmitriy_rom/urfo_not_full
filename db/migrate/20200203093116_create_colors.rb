class CreateColors < ActiveRecord::Migration[5.2]
  def change
    create_table :colors do |t|
      t.string :title,      null: false, unique: true
      t.string :color_code, null: false, unique: true, default: "#000000"

      t.timestamps
    end
  end
end
