class CreateOrderGoods < ActiveRecord::Migration[5.2]
  def change
    create_table :order_goods do |t|
      t.integer :count,         null: false, default: 1

      t.belongs_to :good,       null: false
      t.belongs_to :good_color, null: false
      t.belongs_to :order,      null: false

      t.index [:good_id, :good_color_id, :order_id], unique: true

      t.timestamps
    end
  end
end
