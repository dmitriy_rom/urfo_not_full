class CreateMaterials < ActiveRecord::Migration[5.2]
  def change
    create_table :materials do |t|
      t.string  :title,       null: false
      t.text    :description, null: false
      t.string  :image,       null: false
      t.integer :prior,       null: false, default: 9

      t.timestamps
    end
  end
end
