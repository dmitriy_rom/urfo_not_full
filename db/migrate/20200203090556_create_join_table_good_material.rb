class CreateJoinTableGoodMaterial < ActiveRecord::Migration[5.2]
  def change
    create_join_table :goods, :materials do |t|
      # t.index [:good_id, :material_id]
      # t.index [:material_id, :good_id]
    end
  end
end
