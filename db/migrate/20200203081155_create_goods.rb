class CreateGoods < ActiveRecord::Migration[5.2]
  def change
    create_table :goods do |t|
      t.string  :title,                           null: false
      t.string  :slug,             unique: true,  null: false
      t.integer :height
      t.integer :width
      t.integer :length
      t.text    :description
      t.string  :short_description,               null: false
      t.integer :prior,             default: 9,   null: false
      t.boolean :hided,             default: false
      t.integer :group,             default: 0

      t.belongs_to :series

      t.string :seo_title
      t.string :seo_description
      t.string :seo_keywords

      t.timestamps
    end
  end
end
