class CreateVtrgTables < ActiveRecord::Migration[4.2]
  def change
    create_table :vtrg_tables do |t|
      t.text :body

      t.timestamps
    end
  end
end