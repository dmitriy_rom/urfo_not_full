class CreateVtrgSliderImages < ActiveRecord::Migration[5.2]
  def change
    create_table :vtrg_slider_images do |t|
      t.string :image, null: false
      t.string :title
      t.integer :prior, default: 9, null: false
      t.belongs_to :vtrg_slider

      t.timestamps
    end
  end
end
