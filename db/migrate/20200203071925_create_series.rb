class CreateSeries < ActiveRecord::Migration[5.2]
  def change
    create_table :series do |t|
      t.string  :title,                             null: false
      t.string  :slug,              unique: true,   null: false
      t.text    :description
      t.string  :preview_image,                     null: false
      t.string  :main_image,                        null: false
      t.string  :short_description,                 null: false
      t.integer :prior,             default: 9,     null: false
      t.boolean :hided,             default: false
      t.boolean :vertical,          default: false
      t.integer :group,             default: 0

      t.string :seo_title
      t.string :seo_description
      t.string :seo_keywords

      t.timestamps
    end
  end
end
