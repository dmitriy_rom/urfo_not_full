class CreateVtrgTexts < ActiveRecord::Migration[4.2]
  def change
    create_table :vtrg_texts do |t|
      t.text :body

      t.timestamps
    end
  end
end