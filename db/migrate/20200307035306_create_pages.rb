class CreatePages < ActiveRecord::Migration[5.2]
  def change
    create_table :pages do |t|
      t.string :title, null: false
      t.string :slug

      t.string :seo_title
      t.string :seo_description
      t.string :seo_keywords

      t.timestamps
    end
  end
end
