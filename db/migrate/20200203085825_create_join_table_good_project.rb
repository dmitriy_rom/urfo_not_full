class CreateJoinTableGoodProject < ActiveRecord::Migration[5.2]
  def change
    create_join_table :goods, :projects do |t|
      # t.index [:good_id, :project_id]
      # t.index [:project_id, :good_id]
    end
  end
end
