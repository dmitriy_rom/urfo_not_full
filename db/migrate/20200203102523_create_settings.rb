class CreateSettings < ActiveRecord::Migration[5.2]
  def migrate(direction)
    super
    # Create a default settings
    if direction == :up
      Setting.create([
                         { ident: "site_name",
                           name: "Название сайта",
                           descr: "Название сайта отображается в заголовке страниц сайта",
                           value: "URFO"},
                         { ident: "instagram_link",
                           name: "Ссылка на аккаунт в Instagram",
                           descr: "Будет отображать в подвале сайта",
                           value: "https://instagram.com"},
                         { ident: "facebook_link",
                           name: "Ссылка на аккаунт в facebook",
                           descr: "Будет отображаться в подвале сайта",
                           value: "https://facebook.com"},
                         { ident: "bodyend_codes",
                           name: "HTML перед закрывающим тегом body",
                           descr: "Используется для размещения кодов счётчиков и других технических задач",
                           vtype: Setting::VTYPE_TEXT },
                         { ident: "feedback_email",
                           name: "Эл. почта менеджера для обратной связи",
                           descr: "На эту почту будут отправляться запросы обратной связи",
                           value: "privet@urbanforms.ru" },
                         { ident: "feedback_phone",
                           name: "Телефон менеджера для обратной связи",
                           descr: "Отображается на странице контактов и в форме обратной связи",
                           value: "+7 960 761 50 28" },
                         { ident: "city",
                           name: "Город, в котором находится офис компании",
                           descr: "Отображается на странице контактов",
                           value: "Красноярск" },
                         { ident: "address",
                           name: "Адрес офиса",
                           descr: "Отображается на странице контактов",
                           value: "ул.&nbsp;Ладо Кецховели,&nbsp;22а Бизнес-центр «Спасский», оф.&nbsp;13-03" },
                         { ident: "seo_title",
                           name: "seo-заголовок",
                           descr: "Заголовок страницы по умолчанию",
                           value: "URFO" },
                         { ident: "seo_description",
                           name: "seo-описание",
                           descr: "Описание страницы по умолчанию",
                           value: "Мы придумываем и создаем стильную мебель для городских улиц, парков и скверов" },
                         { ident: "seo_keywords",
                           name: "Ключевые слова",
                           descr: "Ключевые слова по умолчанию",
                           value: "Мебель для улиц, парков, скверов, Красноярск" },
                     ])
    end
  end

  def change
    create_table :settings do |t|
      t.string  :ident,  null: false
      t.string  :name,   null: false
      t.text    :descr
      t.integer :vtype, default: 0
      t.text    :value
      t.boolean :hidden, default: false

      t.timestamps
    end
  end
end
