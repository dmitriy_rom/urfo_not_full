class CreateBasketGoods < ActiveRecord::Migration[5.2]
  def change
    create_table :basket_goods do |t|
      t.integer :count,         null: false, default: 1

      t.belongs_to :good,       null: false
      t.belongs_to :good_color, null: false
      t.belongs_to :basket,     null: false

      t.index [:good_id, :good_color_id, :basket_id], unique: true

      t.timestamps
    end
  end
end
