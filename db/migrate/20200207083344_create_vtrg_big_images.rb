class CreateVtrgBigImages < ActiveRecord::Migration[4.2]
  def change
    create_table :vtrg_big_images do |t|
      t.string :image
      t.string :image_title

      t.timestamps
    end
  end
end