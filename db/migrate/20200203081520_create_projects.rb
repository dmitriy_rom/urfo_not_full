class CreateProjects < ActiveRecord::Migration[5.2]
  def change
    create_table :projects do |t|
      t.string  :title,               null: false
      t.string  :slug,                null: false, unique: true
      t.text    :description
      t.string  :preview_image,       null: false
      t.string  :location_description
      t.integer :prior,               null: false, default: 9
      t.boolean :hided,                            default: false

      t.string :seo_title
      t.string :seo_description
      t.string :seo_keywords

      t.timestamps
    end
  end
end
