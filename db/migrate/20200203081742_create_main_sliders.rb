class CreateMainSliders < ActiveRecord::Migration[5.2]
  def change
    create_table :main_sliders do |t|
      t.integer :prior, default: 9, null: false
      t.belongs_to :project, null: false

      t.timestamps
    end
  end
end
