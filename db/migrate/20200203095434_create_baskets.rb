class CreateBaskets < ActiveRecord::Migration[5.2]
  def change
    create_table :baskets do |t|
      t.string :client_makeshift_id, null: false

      t.timestamps
    end
  end
end
