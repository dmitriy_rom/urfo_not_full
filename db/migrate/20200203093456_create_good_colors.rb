class CreateGoodColors < ActiveRecord::Migration[5.2]
  def change
    create_table :good_colors do |t|
      t.string     :good_image, null: false
      t.integer    :prior,      null: false, default: 9
      t.belongs_to :color,      null: false
      t.belongs_to :good,       null: false

      t.index [:good_id, :color_id], unique: true

      t.timestamps
    end
  end
end
