class CreateVtrgThreeImages < ActiveRecord::Migration[4.2]
  def change
    create_table :vtrg_three_images do |t|
      t.string :first_image
      t.string :second_image
      t.string :third_image

      t.string :first_image_title
      t.string :second_image_title
      t.string :third_image_title

      t.timestamps
    end
  end
end