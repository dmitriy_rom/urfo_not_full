Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  Vitrage.routes(self, controller: 'vitrage_pieces')
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  get '/projects/edit/:slug' => 'projects#edit', as: :edit_project
  get '/pages/edit/:slug' => 'pages#edit', as: :edit_page
  namespace :api, defaults: { format: 'json' } do
    get 'get_good_info/:slug', to: 'goods#show'
    get 'get_sliders', to: 'sliders#index'
    get 'get_small_home_series', to: 'series#get_small_home_series'
    get 'get_big_home_series', to: 'series#get_big_home_series'
    get 'get_series_info/:slug', to: 'series#show'
    get 'get_catalog_items', to: 'catalog#index'
    get 'get_projects', to: 'projects#index'
    get 'get_project_info/:slug', to: 'projects#show'
    get 'get_project_series/:slug', to: 'projects#get_related_series'
    get 'get_series_projects/:slug', to: 'series#get_related_projects'
    get 'get_basket_info', to: 'baskets#show'
    post 'add_basket_good', to: 'baskets#add_basket_good'
    post 'remove_basket_good', to: 'baskets#remove_basket_good'
    post 'send_feedback_request', to: 'feedbacks#feedback_request'
    post 'create_order', to: 'orders#create'
    get 'get_page_info/:slug', to: 'pages#show'
    get 'get_contacts', to: 'main#get_contacts'
    get 'get_seo', to: 'main#get_seo'
    get 'get_project_seo/:slug', to: 'projects#get_project_seo'
    get 'get_page_seo/:slug', to: 'pages#get_seo'
    get 'get_instagram_media', to: 'main#get_instagram_media'
    get 'get_next_project/:slug', to: 'projects#get_next_project'
  end

  get '*page', to: 'static#index', constraints: -> (req) do
    !req.xhr? && req.format.html?
  end

  root 'static#index'
end
